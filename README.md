# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms.

##Author:
 - Joe Webb

##Contact:

- jwebb7@uoregon.edu

##Description:

-This software is used to learn the system to turn in work for CIS 322 and learn basic Git practice.
